const a=document.querySelector.bind(document);
const aa=document.querySelectorAll.bind(document);

const itemssize=aa('.elementsize');
itemssize.forEach((item,index) =>{
    item.onclick=function(){
        a('.elementsize.subspan').classList.remove('subspan');
        this.classList.add('subspan')
    }
})


var imgFeature= a('.img-feature')
var listImg=aa('.product-content-left-small-img img')
var prevBtn =a('.prev');
var nextBtn = a('.next')

var currentIndex = 0;

function updateImageByIndex(index){
    aa('.product-content-left-small-img div').forEach(item =>{
        item.classList.remove('active');
    })

    currentIndex = index;
    imgFeature.src = listImg[index].getAttribute('src')

    listImg[index].parentElement.classList.add('active')
}

listImg.forEach((imgElement,index) =>{
     
    imgElement.addEventListener('click',e =>{
        updateImageByIndex(index);
    })
})

prevBtn.addEventListener('click',e =>{
    if(currentIndex == 0){
        currentIndex = listImg.length - 1;
    }
    else{
        currentIndex--;
    }
    updateImageByIndex(currentIndex)
})

nextBtn.addEventListener('click',e =>{
    if(currentIndex == listImg.length - 1){
        currentIndex = 0;
    }
    else{
        currentIndex++;
    }
    updateImageByIndex(currentIndex)
})