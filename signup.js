const $=document.querySelector.bind(document);
const $$=document.querySelectorAll.bind(document);
var username = $('.username');
var email = $('.email');
var password = $('.password');
var confirmpassword = $('.confirm-password');
var form = $('form')

function showError(input,message) {
    let parent = input.parentElement;
    let small = parent.querySelector('small');
    parent.classList.add('error')
    small.innerHTML = message
}

function showSuccess(input) {
    let parent = input.parentElement;
    let small = parent.querySelector('small');
    parent.classList.remove('error')
    small.innerHTML = ''
}

function checkEmptyInput(...listInput){
    listInput.forEach(input =>{
        input.value = input.value.trim();
        if(!input.value){
            showError(input,'Vui lòng nhập trường này!')
            return false;
        }
  
    })
    return true;
}

function checkEmailInput(input){
    const regexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    input.value = input.value.trim();
    if(regexEmail.test(input.value)) {
        showSuccess(input);
        return true
    }
    showError(input,'Email không hợp lệ!');
   
    return false;
}

function checkLengthError(input,min,max) {
    input.value = input.value.trim();
    if(input.value.length < min ){
        showError(input,'Nhập tối thiểu 6 kí tự')
        return false;
    }
    if(input.value.length > max ){
        showError(input,'Nhập tối đa 15 kí tự')
        return false;
    }
    return true;
}

function checkMathchPasswordError(passwordInput,confirmPasswordInput){
    if(passwordInput.value !== confirmPasswordInput.value){
        showError(confirmPasswordInput,'Mật khẩu không trùng khớp!')
        return false;
    }
    return true;
}

form.addEventListener('submit', e=>{
    
    e.preventDefault()
    let isEmptyOk = checkEmptyInput(username,email,password,confirmpassword)
    let isEmailOk = checkEmailInput(email) 
    let isUsernameLengthError = checkLengthError(username,6 ,15)
    let isPasswordLengthError = checkLengthError(password,6 ,15)
    let isMatchError = checkMathchPasswordError(password,confirmpassword)
    console.log(isEmptyOk,isEmailOk,isUsernameLengthError,isPasswordLengthError,isMatchError)
    if(isEmailOk&&isEmptyOk&&isUsernameLengthError&&isPasswordLengthError&&isMatchError){
        console.log("ok");
        const { protocol, host } = window.location
        window.location.href = protocol + '//' + host + '/signin.html'
    }else{
        alert("ko")
    }
    
})