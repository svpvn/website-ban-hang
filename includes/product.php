<?php
include "header.php";
?>
<div class="product">
    <div class="container">
        <div class="product-top row">
            <p>Trang chủ</p> <span>&#8260;</span>
            <p>Áo</p><span>&#8260;</span>
            <p>TEE</p> <span>&#8260;</span>
            <p>1 Year Tee - Black</p>
        </div>
    </div>
    <div class="product-content row">
        <?php
        include "../admin/config.php";
        $result = mysqli_query($conn, "SELECT * FROM `tbl_product` WHERE `product_id`=" . $_GET['id'] . "");
        $product = mysqli_fetch_assoc($result);

        $img_desc = mysqli_query($conn, "SELECT * FROM `tbl_product_img_desc` WHERE `product_id`=" . $_GET['id'] . "");
        $product['images'] = mysqli_fetch_all($img_desc, MYSQLI_ASSOC);




        // $totalRecords = mysqli_query($conn, "SELECT * FROM tbl_product");
        // $totalRecords = $totalRecords->num_rows;
        // $totalPages = ceil($totalRecords / $item_per_page);
        ?>
        <div class="product-content-left row">
            <div class="product-content-left-small-img">


                <div><img src="../assets/img/imgsmall1.jpg"></div>
                <div><img src="../assets/img/imgsmall2.jpg"></div>
                <div><img src="../assets/img/imgsmall3.jpg"></div>
                <div><img src="../assets/img/imgsmall4.jpg"></div>



            </div>
            <div class="product-content-left-big-img">
                <img src="../admin/uploads/<?php echo $product['product_img'] ?>" alt="" class="img-feature">
                <div class="control prev">
                    <i class="fa-solid fa-chevron-left"></i>
                </div>
                <div class="control next">
                    <i class="fa-solid fa-chevron-right"></i>
                </div>
            </div>
        </div>
        <div class="product-content-right">
            <div class="product-content-right-name borderbottom">
                <h2><?php echo $product['product_name'] ?></h2>
                <p>SKU: ORP163 - S</p>
            </div>
            <div class="product-content-right-price borderbottom">
                <p><?php echo $product['product_price'] ?> <sub>₫</sub></p>
            </div>
            <div class="product-content-right-size borderbottom">
                <div class="size">
                    <div class="elementsize subspan"><span>S</span></div>
                    <div class="elementsize"><span>M</span></div>
                    <div class="elementsize"><span>L</span></div>
                </div>
            </div>
            <div class="product-content-right-color borderbottom">
                <p>Màu đen</p>
                <div class="product-content-right-color-img">
                    <img src="../assets/img/images.png" alt="">
                </div>
            </div>
            <div class="quantity row">
                <p style="font-weight: bold;">Số lượng:</p>
                <input type="number" min="0" value="1">
            </div>
            <div class="product-content-right-button">
                <a href="cart.php"> <button>Thêm Vào Giỏ</button></a>
            </div>
            <div class="product-content-right-container">
                <h2>Mô tả</h2>
                <ul>
                    <li>
                        <p>Gồm 3 size: S / M / L </p>
                    </li>
                    <li>
                        <p>Thông số áo: S : Dài 69 Rộng 52.5 | M : Dài 73 Rộng 55 | L: Dài : 76.5 Rộng: 57.5</p>
                    </li>
                    <li>
                        <p>Chất liệu vải :100% Cotton '' Chất lượng vải đánh dấu sự ra đời của Outerity''</p>
                    </li>
                    <li>
                        <p>Công nghệ in: In lụa cao cấp, bảo quản tốt khi giặt máy , không bong tróc phai màu</p>
                    </li>
                    <li>
                        <p>Outerity xuất hiện mang đến làn gió mới, xuất hiện từ đầu năm 2021 đầy biến động nhưng những
                            gì Outerity thực hiện đem tới một chất lượng và giá thành tốt nhất đến cho các bạn nha.</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Sản phẩm liên quan -->
    <div class="product-related">
        <div class="product-related-title">
            <p>SẢN PHẨM LIÊN QUAN</p>
        </div>
        <div class="product-related-content row">
            <?php
            $conn = new mysqli('localhost', 'root', '', 'website_outerity');
            $sql = "SELECT * FROM tbl_product";
            $result = $conn->query($sql);
            ?>
            <?php
            $num = 5;
            while ($row = $result->fetch_assoc()) :

            ?>
            <?php if ($num > 0) : ?>
            <?php $num = $num - 1 ?>
            <div class="product-related-content-items">
                <a href="product.html">
                    <img src="../admin/uploads/<?php echo $row['product_img'] ?>" alt="">
                    <h1> <?php echo $row['product_name'] ?> </h1>
                    <p><?php echo $row['product_price'] ?><sup>₫</sup></p>
                </a>

            </div>
            <?php endif ?>

            <?php endwhile ?>
            <!-- <div class="product-related-content-items">
                <img src="../assets/img/product-related-content-item2.jpg" alt="">
                <h1>BabyFont Tee v2.0 - White Color</h1>
                <p>180,000 <sub>₫</sub></p>
            </div>
            <div class="product-related-content-items">
                <img src="../assets/img/product-related-content-item3.jpg" alt="">
                <h1>BabyFont Tee v2.0 - White Color</h1>
                <p>180,000 <sub>₫</sub></p>
            </div>
            <div class="product-related-content-items">
                <img src="../assets/img/product-related-content-item4.jpg" alt="">
                <h1>BabyFont Tee v2.0 - White Color</h1>
                <p>180,000 <sub>₫</sub></p>
            </div>
            <div class="product-related-content-items">
                <img src="../assets/img/product-related-content-item5.jpg" alt="">
                <h1>BabyFont Tee v2.0 - White Color</h1>
                <p>180,000 <sub>₫</sub></p>
            </div> -->
        </div>
    </div>
</div>
<div class="product__seemore">
    <button><a href="cartegory.php">Xem thêm</a></button>
</div>
<!-- FOOTER -->
<div class="footer">
    <div class="footer-top">
        <p><i href="" class="fa fa-phone"></i> Hỗ trợ <span>&#8260;</span> Mua hàng: <a href="">0348888888</a></p>
    </div>
    <div class="footer-bottom row">
        <div class="footer-bottom-column-one">
            <h3>Giới thiệu</h3>
            <img src="../assets/img/logo_bct_019590229b4c4dfda690236b67f7aff4.png" alt="">
        </div>
        <div class="footer-bottom-column-two">
            <h3>Liên kết</h3>
            <p>Tìm kiếm</p>
            <p>Giới thiệu</p>
            <p>Chính sách đổi trả</p>
            <p>Chính sách bảo mật</p>
            <p>Điều khoản dịch vụ</p>
        </div>
        <div class="footer-bottom-column-three">
            <h3>Thông tin liên hệ</h3>
            <p><i class="fa-solid fa-location-dot"></i>TP.HCM</p>
            <p><i class="fa-solid fa-phone"></i>0348888888</p>
            <p><i class="fa-solid fa-envelope"></i>outerity.local@gmail.com</p>
        </div>
        <div class="footer-bottom-column-four">
            <h3>FANPAGE</h3>
            <iframe
                src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fouterity&tabs&width=350&height=250&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId"
                width="350" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                allowfullscreen="true"
                allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
        </div>
    </div>
</div>
</body>
<script src="../products.js"></script>

</html>