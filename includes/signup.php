<?php
include "header.php";
?>

<div class="signup">
    <div class="signup-container row">
        <div class="signup-left">
            <img src="../assets/img/outerity.png" alt="">
        </div>
        <div class="signup-right">
            <form action="register_submit.php" method="POST">
                <h1>Đăng Ký</h1>
                <div class="form-control ">
                    <input type="text" placeholder="Tài khoản" class="username" name="username">
                    <small></small>
                    <span></span>
                </div>
                <div class="form-control ">
                    <input type="email" placeholder="Email" class="email" name="email">
                    <small></small>
                    <span></span>
                </div>
                <div class="form-control ">
                    <input type="password" placeholder="Mật khẩu" class="password" name="password">
                    <small></small>
                    <span></span>
                </div>
                <div class="form-control ">
                    <input type="password" placeholder="Nhập lại mật khẩu" class="confirm-password" name="repassword">
                    <small></small>
                    <span></span>
                </div>
                <div style="text-align:center"><button type="submit" name="submit" class="btn-submit">Đăng ký</button>
                </div>
                <div class="signup-link">Bạn đã có tài khoản? <a href="signin.php">Đăng nhập</a></div>
            </form>
        </div>
    </div>
    <!-- <script src="../signup.js"></script> -->

</div>
<!-- footer -->
<div class="footer">
    <div class="footer-top">
        <p><i href="" class="fa fa-phone"></i>Hỗ trợ <span>&#8260;</span> Mua hàng: <a href="">0348888888</a></p>
    </div>
    <div class="footer-bottom row">
        <div class="footer-bottom-column-one">
            <h3>Giới thiệu</h3>
            <img src="../assets/img/logo_bct_019590229b4c4dfda690236b67f7aff4.png" alt="">
        </div>
        <div class="footer-bottom-column-two">
            <h3>Liên kết</h3>
            <p>Tìm kiếm</p>
            <p>Giới thiệu</p>
            <p>Chính sách đổi trả</p>
            <p>Chính sách bảo mật</p>
            <p>Điều khoản dịch vụ</p>
        </div>
        <div class="footer-bottom-column-three">
            <h3>Thông tin liên hệ</h3>
            <p><i class="fa-solid fa-location-dot"></i>TP.HCM</p>
            <p><i class="fa-solid fa-phone"></i>0348888888</p>
            <p><i class="fa-solid fa-envelope"></i>outerity.local@gmail.com</p>
        </div>
        <div class="footer-bottom-column-four">
            <h3>FANPAGE</h3>
            <iframe
                src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fouterity&tabs&width=350&height=250&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId"
                width="350" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                allowfullscreen="true"
                allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
        </div>
    </div>
</div>

</body>

</html>