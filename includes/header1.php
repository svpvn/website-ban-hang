<?php
include "header.php"
?>
<div class="swiper">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->

        <div class="swiper-slide"><img src="../assets/img/slider.jpg" alt=""></div>
        <div class="swiper-slide"><img src="../assets/img/outeri.jpg" alt=""></div>
        <div class="swiper-slide"><img src="../assets/img/outerity2.png" alt=""></div>

    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination"></div>

    <!-- If we need navigation buttons -->
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>

    <!-- If we need scrollbar -->
    <div class="swiper-scrollbar"></div>
</div>

<script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>
<script>
const swiper = new Swiper('.swiper', {
    // Optional parameters


    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
});
</script>
<!-- product -->
<div class="product-related">
    <div class="product-related-title">
        <p>SẢN PHẨM</p>
    </div>
    <div class="product-related-content row">
        <?php
        $conn = new mysqli('localhost', 'root', '', 'website_outerity');
        $sql = "SELECT * FROM tbl_product";
        $result = $conn->query($sql);
        ?>
        <?php
        $num = 10;
        while ($row = $result->fetch_assoc()) :

        ?>
        <?php if ($num > 0) : ?>
        <?php $num = $num - 1 ?>
        <div class="product-related-content-items">
            <a href="product.html">
                <img src="../admin/uploads/<?php echo $row['product_img'] ?>" alt="">
                <h1> <?php echo $row['product_name'] ?> </h1>
                <p><?php echo $row['product_price'] ?><sup>₫</sup></p>
            </a>

        </div>
        <?php endif ?>

        <?php endwhile ?>


        <!-- <div class="product-related-content-items">
            <img src="../assets/img/product-related-content-item1.jpg" alt="">
            <h1>BabyFont Tee v2.0 - White Color</h1>
            <p>180,000 <sub>₫</sub></p>
        </div>
        <div class="product-related-content-items">
            <img src="../assets/img/product-related-content-item2.jpg" alt="">
            <h1>BabyFont Tee v2.0 - White Color</h1>
            <p>180,000 <sub>₫</sub></p>
        </div>
        <div class="product-related-content-items">
            <img src="../assets/img/product-related-content-item3.jpg" alt="">
            <h1>BabyFont Tee v2.0 - White Color</h1>
            <p>180,000 <sub>₫</sub></p>
        </div>
        <div class="product-related-content-items">
            <img src="../assets/img/product-related-content-item4.jpg" alt="">
            <h1>BabyFont Tee v2.0 - White Color</h1>
            <p>180,000 <sub>₫</sub></p>
        </div>
        <div class="product-related-content-items">
            <img src="../assets/img/product-related-content-item5.jpg" alt="">
            <h1>BabyFont Tee v2.0 - White Color</h1>
            <p>180,000 <sub>₫</sub></p>
        </div>
        <div class="product-related-content-items">
            <img src="../assets/img/product-related-content-item5.jpg" alt="">
            <h1>BabyFont Tee v2.0 - White Color</h1>
            <p>180,000 <sub>₫</sub></p>
        </div>
        <div class="product-related-content-items">
            <img src="../assets/img/product-related-content-item5.jpg" alt="">
            <h1>BabyFont Tee v2.0 - White Color</h1>
            <p>180,000 <sub>₫</sub></p>
        </div>
        <div class="product-related-content-items">
            <img src="../assets/img/product-related-content-item5.jpg" alt="">
            <h1>BabyFont Tee v2.0 - White Color</h1>
            <p>180,000 <sub>₫</sub></p>
        </div>
        <div class="product-related-content-items">
            <img src="../assets/img/product-related-content-item5.jpg" alt="">
            <h1>BabyFont Tee v2.0 - White Color</h1>
            <p>180,000 <sub>₫</sub></p>
        </div>
        <div class="product-related-content-items">
            <img src="../assets/img/product-related-content-item5.jpg" alt="">
            <h1>BabyFont Tee v2.0 - White Color</h1>
            <p>180,000 <sub>₫</sub></p>
        </div> -->
    </div>
</div>
<!-- xem them -->
<div class="product__seemore">
    <button><a href="cartegory.php">Xem thêm</a></button>
</div>
<!-- footer -->
<div class="footer">
    <div class="footer-top">
        <p><i href="" class="fa fa-phone"></i>Hỗ trợ <span>&#8260;</span> Mua hàng: <a href="">0348888888</a></p>
    </div>
    <div class="footer-bottom row">
        <div class="footer-bottom-column-one">
            <h3>Giới thiệu</h3>
            <img src="../assets/img/logo_bct_019590229b4c4dfda690236b67f7aff4.png" alt="">
        </div>
        <div class="footer-bottom-column-two">
            <h3>Liên kết</h3>
            <p>Tìm kiếm</p>
            <p>Giới thiệu</p>
            <p>Chính sách đổi trả</p>
            <p>Chính sách bảo mật</p>
            <p>Điều khoản dịch vụ</p>
        </div>
        <div class="footer-bottom-column-three">
            <h3>Thông tin liên hệ</h3>
            <p><i class="fa-solid fa-location-dot"></i>TP HN</p>
            <p><i class="fa-solid fa-phone"></i>0348888888</p>
            <p><i class="fa-solid fa-envelope"></i>outerity.local@gmail.com</p>
        </div>
        <div class="footer-bottom-column-four">
            <h3>FANPAGE</h3>
            <iframe
                src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fouterity&tabs&width=350&height=250&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId"
                width="350" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                allowfullscreen="true"
                allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
        </div>
    </div>
</div>
</body>

</html>