<?php
include "header.php";
?>
<div class="delivery">
    <div class="cart-top row">
        <p>Trang chủ</p> <span>&#8260;</span>
        <p>Giao hàng</p>
    </div>
    <div class="main">
        <div class="heading-page">
            <div class="header-page">
                <h1>Địa chỉ giao hàng</h1>
            </div>
        </div>
        <div class="delivery-content row">
            <div class="delivery-content-left">
                <div class="header-delivery">
                    <h1>Outerity</h1>
                    <ul class="breadcrumb row">
                        <li class="breadcrumb-item">
                            <a href="">Giỏ hàng</a>
                        </li>
                        <li class="breadcrumb-item-current">Thông tin giao hàng</li>
                    </ul>
                </div>
                <div class="content-delivery-signin">
                    <div class="content-delivery-left">
                        <div class="content-delivery-header">
                            <h2>Thông tin giao hàng</h2>
                        </div>
                        <div class="content-delivery-header-text">
                            <p>Bạn đã có tài khoản?<a href="">Đăng nhập</a></p>
                        </div>
                    </div>
                </div>
                <div class="delivery-content-left-input-top row">
                    <div class="delivery-content-left-input-top-item">
                        <label for="">Họ tên <span style="color:red">*</span></label>
                        <input type="text">
                    </div>
                    <div class="delivery-content-left-input-top-item">
                        <label for="">Điện thoại <span style="color:red">*</span></label>
                        <input type="text">
                    </div>
                    <div class="delivery-content-left-input-top-item">
                        <label for="">Tỉnh/Thành phố <span style="color:red">*</span></label>
                        <input type="text">
                    </div>
                    <div class="delivery-content-left-input-top-item">
                        <label for="">Quận/Huyện <span style="color:red">*</span></label>
                        <input type="text">
                    </div>
                </div>
                <div class="delivery-content-left-input-bottom">
                    <label for="">Địa chỉ <span style="color:red">*</span></label>
                    <input type="text">
                </div>
                <div class="delivery-content-left-button row">
                    <a href=""><span>&#171;</span>
                        <p>Quay lại giỏ hàng</p>
                    </a>
                    <a href="signup.php"><button style="font-weight:bold">THANH TOÁN VÀ GIAO HÀNG</button></a>
                </div>
                <!-- payment -->
                <div class="payment-content-left">
                    <h3 class="checkout-title">Phương thức thanh toán</h3>
                    <div class="block-border">
                        <p>Mọi giao dịch đều được bảo mật và mã hóa. Thông tin thẻ tín dụng sẽ không bao giờ được lưu
                            lại.</p>
                        <div class="payment-content-left-options">
                            <label for="" class="ds__item">
                                <input class="ds__item__input" type="radio" name="payment_method" id="payment_method_1"
                                    value="1">
                                <span class="ds__item__label">Thanh toán bằng thẻ tín dụng</span>
                                <span><img src="./assets/img/1.png" alt=""></span>
                            </label>
                            <label for="" class="ds__item">
                                <input class="ds__item__input" type="radio" name="payment_method" id="payment_method_2"
                                    value="2">
                                <span class="ds__item__label">Thanh toán thẻ ATM</span>

                            </label>
                            <label for="" class="ds__item">
                                <input class="ds__item__input" type="radio" name="payment_method" id="payment_method_3"
                                    value="3">
                                <span class="ds__item__label">Thanh khi giao hàng</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="delivery-content-right">
                <table>
                    <tr>
                        <th>Tên sản phẩm</th>
                        <th> Số lượng</th>
                        <th>Thành tiền</th>
                    </tr>
                    <tr>
                        <td>1 Year Tee - Black</td>
                        <td>1</td>
                        <td>
                            <p>180.000 <sub>₫</sub></p>
                        </td>
                    </tr>
                    <tr>
                        <td>1 Year Tee - Black</td>
                        <td>2</td>
                        <td>
                            <p>180.000 <sub>₫</sub></p>
                        </td>
                    </tr>
                    <tr class="">
                        <td style="font-weight: bold;" colspan="2">Tổng</td>
                        <td style="font-weight: bold;">
                            <p>180.000 <sub>₫</sub></p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER -->
<div class="footer">
    <div class="footer-top">
        <p><i href="" class="fa fa-phone"></i> Hỗ trợ <span>&#8260;</span> Mua hàng: <a href="">0348888888</a></p>
    </div>
    <div class="footer-bottom row">
        <div class="footer-bottom-column-one">
            <h3>Giới thiệu</h3>
            <img src="/assets/img/logo_bct_019590229b4c4dfda690236b67f7aff4.png" alt="">
        </div>
        <div class="footer-bottom-column-two">
            <h3>Liên kết</h3>
            <p>Tìm kiếm</p>
            <p>Giới thiệu</p>
            <p>Chính sách đổi trả</p>
            <p>Chính sách bảo mật</p>
            <p>Điều khoản dịch vụ</p>
        </div>
        <div class="footer-bottom-column-three">
            <h3>Thông tin liên hệ</h3>
            <p><i class="fa-solid fa-location-dot"></i>TP.HCM</p>
            <p><i class="fa-solid fa-phone"></i>0348888888</p>
            <p><i class="fa-solid fa-envelope"></i>outerity.local@gmail.com</p>
        </div>
        <div class="footer-bottom-column-four">
            <h3>FANPAGE</h3>
            <iframe
                src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fouterity&tabs&width=350&height=250&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId"
                width="350" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                allowfullscreen="true"
                allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
        </div>
    </div>
</div>
</body>

</html>