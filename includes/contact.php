<?php
include "header.php";
?>
<section>
    <div class="container">
        <div class="containerinfo">
            <div>
                <h2>Thông Tin Liên Hệ</h2>
                <ul class="info">
                    <li>
                        <span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                        <span> Nguyễn Trãi<br />
                            Hà Đông,<br />
                            Thành Phố Hà Nội
                        </span>
                    </li>
                    <li>
                        <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                        <span>nhom5@gmail.com</span>
                    </li>

                    <li>
                        <span><i class="fa fa-phone-square" aria-hidden="true"></i></span>
                        <span>012-345-6789</span>
                    </li>
                </ul>
            </div>
            <ul class="sci">
                <li><a href="#"><i class="fa-brands fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa-brands fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa-brands fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa-brands fa-pinterest" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa-brands fa-linkedin" aria-hidden="true"></i></a></li>
            </ul>
        </div>
        <div class="containerForm">
            <h2>Gửi Lời Nhắn</h2>
            <div class="formBox">
                <div class="inputBox w50">
                    <input type="text" name="" required>
                    <span>Họ</span>
                </div>
                <div class="inputBox w50">
                    <input type="text" name="" required>
                    <span>Tên</span>
                </div>
                <div class="inputBox w50">
                    <input type="text" name="" required>
                    <span>Email</span>
                </div>
                <div class="inputBox w50">
                    <input type="text" name="" required>
                    <span>Số Điện Thoại</span>
                </div>
                <div class="inputBox w100">
                    <textarea name="" required></textarea>
                    <span>Lời Nhắn Của Bạn</span>
                </div>
                <div class="inputBox w100">
                    <input type="submit" value="Gửi">
                </div>
            </div>
        </div>
</section>
<!-- footer -->
<div class="footer">
    <div class="footer-top">
        <p><i href="" class="fa fa-phone"></i>Hỗ trợ :<a href="">0348888888</a></p>
    </div>
    <div class="footer-bottom row">
        <div class="footer-bottom-column-one">
            <h3>Giới thiệu</h3>
            <img src="../assets/img/logo_bct_019590229b4c4dfda690236b67f7aff4.png" alt="">
        </div>
        <div class="footer-bottom-column-two">
            <h3>Liên kết</h3>
            <p>Tìm kiếm</p>
            <p>Giới thiệu</p>
            <p>Chính sách thuê truyện</p>
            <p>Chính sách bảo mật</p>
            <p>Điều khoản dịch vụ</p>
        </div>
        <div class="footer-bottom-column-three">
            <h3>Thông tin liên hệ</h3>
            <p><i class="fa-solid fa-location-dot"></i>TP.Hà Nội</p>
            <p><i class="fa-solid fa-phone"></i>038888888</p>
            <p><i class="fa-solid fa-envelope"></i> outerity@gmail.com</p>
        </div>
        <div class="footer-bottom-column-four">
            <h3>FANPAGE</h3>
            <iframe
                src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fouterity&tabs&width=350&height=250&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId"
                width="350" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                allowfullscreen="true"
                allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
        </div>
    </div>
</div>
</div>
</body>

</html>