<?php
include "header.php";

?>
<div class="cart">
    <div class="cart-top row">
        <p>Trang chủ</p> <span>&#8260;</span>
        <p>Giỏ hàng</p>
    </div>
    <div class="container-cart">
        <div class="heading-page">
            <div class="header-page">
                <h1>Giỏ hàng của bạn</h1>
            </div>
        </div>
        <div class="cart-content row">
            <div class="container-cart-left">
                <table>
                    <tr>
                        <th>Sản phẩm</th>
                        <th>Tên sản phẩm</th>
                        <th>Màu</th>
                        <th>Size</th>
                        <th>SL</th>
                        <th>Thành tiền</th>
                        <th>Xóa</th>
                    </tr>
                    <tr>
                        <td><img src="../assets/img/cart-product1.jpg" alt=""></td>
                        <td>
                            <p>BabyFont Tee v2.0 - White Color</p>
                        </td>
                        <td>
                            <p>Trắng</p>
                        </td>
                        <td>
                            <p>S</p>
                        </td>
                        <td><input type="number" value="1" min="1"></td>
                        <td>
                            <p>180,000<sub>₫</sub></p>
                        </td>
                        <td><span>X</span></td>
                    </tr>
                    <tr>
                        <td><img src="../assets/img/cart-product2.jpg" alt=""></td>
                        <td>
                            <p>BabyFont Tee v2.0 - White Color</p>
                        </td>
                        <td>
                            <p>Trắng</p>
                        </td>
                        <td>
                            <p>S</p>
                        </td>
                        <td><input type="number" value="1" min="1"></td>
                        <td>
                            <p>180,000<sub>₫</sub></p>
                        </td>
                        <td><span>X</span></td>
                    </tr>
                    <tr>
                        <td><img src="../assets/img/cart-product3.jpg" alt=""></td>
                        <td>
                            <p>BabyFont Tee v2.0 - White Color</p>
                        </td>
                        <td>
                            <p>Trắng</p>
                        </td>
                        <td>
                            <p>S</p>
                        </td>
                        <td><input type="number" value="1" min="1"></td>
                        <td>
                            <p>180,000<sub>₫</sub></p>
                        </td>
                        <td><span>X</span></td>
                    </tr>
                </table>
                <div class="container-cart-left-text">
                    <ul>
                        <h3>Chính sách mua hàng</h3>
                        <li>Sản phẩm được đổi 1 lần duy nhất, không hỗ trợ trả.</li>
                        <li>Sản phẩm còn đủ tem mác, chưa qua sử dụng.</li>
                        <li>Sản phẩm nguyên giá được đổi trong 30 ngày trên toàn hệ thống</li>
                        <li>Sản phẩm sale chỉ hỗ trợ đổi size (nếu cửa hàng còn) trong 7 ngày trên toàn hệ thống</li>
                    </ul>
                </div>
            </div>
            <div class="container-cart-right">
                <table>
                    <tr>
                        <th colspan="2">TỔNG TIỀN GIỎ HÀNG</th>
                    </tr>
                    <tr>
                        <td>Tổng sản phẩm</td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td>Tổng tiền hàng</td>
                        <td>
                            <p>180,000<sub>₫</sub></p>
                        </td>
                    </tr>
                </table>
                <div class="container-cart-right-text">
                    <p>Phí vận chuyển sẽ được tính ở trang thanh toán.<br>
                        Bạn cũng có thể nhập mã giảm giá ở trang thanh toán.</p>
                </div>
                <div class="container-cart-right-button">
                    <button> <a href="delivery.php"> THANH TOÁN</a></button>
                    <a href="cartegory.php"> <button><i class="fa-solid fa-reply"></i>Tiếp tục mua hàng</button></a>
                </div>
                <div class="container-cart-right-signin">
                    <p>Tài khoản Outerity</p>
                    <p>Hãy <a href="signin.php" style="color: red;"> Đăng nhập </a>tài khoản của bạn</p>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- FOOTER -->
<div class="footer">
    <div class="footer-top">
        <p><i href="" class="fa fa-phone"></i> Hỗ trợ <span>&#8260;</span> Mua hàng: <a href="">0348888888</a></p>
    </div>
    <div class="footer-bottom row">
        <div class="footer-bottom-column-one">
            <h3>Giới thiệu</h3>
            <img src="/assets/img/logo_bct_019590229b4c4dfda690236b67f7aff4.png" alt="">
        </div>
        <div class="footer-bottom-column-two">
            <h3>Liên kết</h3>
            <p>Tìm kiếm</p>
            <p>Giới thiệu</p>
            <p>Chính sách đổi trả</p>
            <p>Chính sách bảo mật</p>
            <p>Điều khoản dịch vụ</p>
        </div>
        <div class="footer-bottom-column-three">
            <h3>Thông tin liên hệ</h3>
            <p><i class="fa-solid fa-location-dot"></i>TP.HCM</p>
            <p><i class="fa-solid fa-phone"></i>0348888888</p>
            <p><i class="fa-solid fa-envelope"></i>outerity.local@gmail.com</p>
        </div>
        <div class="footer-bottom-column-four">
            <h3>FANPAGE</h3>
            <iframe
                src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fouterity&tabs&width=350&height=250&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId"
                width="350" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                allowfullscreen="true"
                allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
        </div>
    </div>
</div>
</body>

</html>