<?php
include "header.php";
?>
<?php
$conn = new mysqli('localhost', 'root', '', 'website_outerity');
$item_per_page = !empty($_GET['per_page']) ? $_GET['per_page'] : 8;
$current_page = !empty($_GET['page']) ? $_GET['page'] : 1;
$offset = ($current_page - 1) * $item_per_page;
$orderConditon = "";
$orderField = isset($_GET['field']) ? $_GET['field'] : "";
$orderSort = isset($_GET['sort']) ? $_GET['sort'] : "";
if (!empty($orderField) && !empty($orderSort)) {
    $orderConditon = "ORDER BY `tbl_product`.`" . $orderField . "` " . $orderSort . "";
}
$sql = "SELECT * FROM `tbl_product` " . $orderConditon . " LIMIT " . $item_per_page . " OFFSET " . $offset . "";
$result = $conn->query($sql);
$totalRecords = mysqli_query($conn, "SELECT * FROM tbl_product");
$totalRecords = $totalRecords->num_rows;
$totalPages = ceil($totalRecords / $item_per_page);
?>
<div class="category">
    <div class="container">
        <div class="container-top row">
            <p>Trang chủ</p> <span>&#8260;</span>
            <p>Áo</p><span>&#8260;</span>
            <p>TEE</p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="category-left">
                <ul>
                    <li class="category-left-list block">
                        <a href="#">TOPS <span>&#8722;</span> </a>
                        <ul>
                            <li><a href="">TEE</a></li>
                            <li><a href="">POLO</a></li>
                            <li><a href="">HOODIE</a></li>
                        </ul>
                    </li>
                    <li class="category-left-list">
                        <a href="#">BOTTOM <span>&#8722;</span></a>
                        <ul>
                            <li><a href="">SHORT</a></li>
                        </ul>
                    </li>
                    <li class="category-left-list">
                        <a href="#">ACCESSORIES <span>&#8722;</span></a>
                        <ul>
                            <li><a href="">TOTE BAG</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="category-right ">
                <div class="row">
                    <div class="category-right-top-item">
                        <p>TEE</p>
                    </div>
                    <div class="category-right-top-item">
                        <button><span>Bộ lọc</span><i class="fa-solid fa-sort-down"></i></button>
                    </div>
                    <div class="category-right-top-item">
                        <select name="" id=""
                            onchange="this.options[this.selectedIndex].value && (window.location=this.options[this.selectedIndex].value);">
                            <option value="">Sắp xếp giá</option>
                            <option value="?field=product_price&sort=desc">Giá cao đến thấp</option>
                            <option value="?field=product_price&sort=asc">Giá thấp đến cao</option>
                        </select>
                    </div>
                </div>

                <div class="category-right-content">

                    <?php

                    while ($row = $result->fetch_assoc()) :

                    ?>

                    <div class="category-right-content-item">

                        <a href="product.php?id=<?php echo $row['product_id'] ?>"><img
                                src="../admin/uploads/<?php echo $row['product_img'] ?>" alt=""></a>
                        <h1> <a
                                href="product.php?id=<?php echo $row['product_id'] ?>"><?php echo $row['product_name'] ?></a>
                        </h1>
                        <p><?php echo $row['product_price'] ?><sup>₫</sup></p>


                    </div>

                    <?php endwhile ?>


                    <!-- <div class="category-right-content-item">
                        <img src="../assets/img/tee2.jpg" alt="">
                        <h1>1 Year Tee - Black</h1>
                        <p>180.000<sup>₫</sup></p>
                    </div>
                    <div class="category-right-content-item">
                        <img src="../assets/img/tee3.jpg" alt="">
                        <h1>1 Year Tee - Black</h1>
                        <p>180.000<sup>₫</sup></p>
                    </div>
                    <div class="category-right-content-item">
                        <img src="../assets/img/tee4.jpg" alt="">
                        <h1>1 Year Tee - Black</h1>
                        <p>180.000<sup>₫</sup></p>
                    </div>
                    <div class="category-right-content-item">
                        <img src="../assets/img/tee5.jpg" alt="">
                        <h1>1 Year Tee - Black</h1>
                        <p>180.000<sup>₫</sup></p>
                    </div>
                    <div class="category-right-content-item">
                        <img src="../assets/img/tee6.jpg" alt="">
                        <h1>1 Year Tee - Black</h1>
                        <p>180.000<sup>₫</sup></p>
                    </div>
                    <div class="category-right-content-item">
                        <img src="../assets/img/tee7.jpg" alt="">
                        <h1>1 Year Tee - Black</h1>
                        <p>180.000<sup>₫</sup></p>
                    </div>
                    <div class="category-right-content-item">
                        <img src="../assets/img/tee8.jpg" alt="">
                        <h1>1 Year Tee - Black</h1>
                        <p>180.000<sup>₫</sup></p>
                    </div> -->
                </div>

                <div class="category-right-bottom">
                    <div class="category-right-bottom-items">
                        <?php for ($num = 1; $num <= $totalPages; $num++) :
                        ?>

                        <a href="?per_page=<?= $item_per_page ?>&page=<?= $num ?>"><?= $num ?></a>

                        <?php endfor ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="../cartegory.js"></script>

<!-- footer -->
<div class="footer">
    <div class="footer-top">
        <p><i href="" class="fa fa-phone"></i>Hỗ trợ <span>&#8260;</span> Mua hàng: <a href="">0348888888</a></p>
    </div>
    <div class="footer-bottom row">
        <div class="footer-bottom-column-one">
            <h3>Giới thiệu</h3>
            <img src="../assets/img/logo_bct_019590229b4c4dfda690236b67f7aff4.png" alt="">
        </div>
        <div class="footer-bottom-column-two">
            <h3>Liên kết</h3>
            <p>Tìm kiếm</p>
            <p>Giới thiệu</p>
            <p>Chính sách đổi trả</p>
            <p>Chính sách bảo mật</p>
            <p>Điều khoản dịch vụ</p>
        </div>
        <div class="footer-bottom-column-three">
            <h3>Thông tin liên hệ</h3>
            <p><i class="fa-solid fa-location-dot"></i>TP.HCM</p>
            <p><i class="fa-solid fa-phone"></i>0348888888</p>
            <p><i class="fa-solid fa-envelope"></i>outerity.local@gmail.com</p>
        </div>
        <div class="footer-bottom-column-four">
            <h3>FANPAGE</h3>
            <iframe
                src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fouterity&tabs&width=350&height=250&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId"
                width="350" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                allowfullscreen="true"
                allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
        </div>
    </div>
</div>
</body>

</html>